class AddStatusToPurchases < ActiveRecord::Migration
  def change
    add_column :purchases, :status, :integer
  end
end
