class Purchase < ActiveRecord::Base
  def self.user_purchases(id)
    self.where("user_id = ?", id)
  end
end
