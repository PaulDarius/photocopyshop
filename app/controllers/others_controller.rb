class OthersController < ApplicationController
  before_action :authenticate_user!
  before_filter :is_admin?
  # GET /purchases
  # GET /purchases.json
  def is_admin?
    if current_user.admin?
      true
    else
      false
    end
  end

  def index

  end

  def users

  end

  def show

  end
end